package main

import (
  "net/http"
  "fmt"
  "log"

  "github.com/joho/godotenv"

  "golang-api/routers"
  "golang-api/database"
  "golang-api/models"
)

// @title Golang Gin API
// @description An example of gin
// @version 1.0
// @host localhost:8080
// @BasePath /
//
// @securityDefinitions.apikey	ApiKeyAuth
// @in							header
// @name						Authorization
// @description			Value欄位中輸入格式如下「Bearer Your_JWT_Token」
func main() {
	loadEnv()
	loadDatabase()
	serveApplication()
}

func loadEnv() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func loadDatabase() {
	database.Connect()
	database.Database.AutoMigrate(&models.User{})
	database.Database.AutoMigrate(&models.Entry{})
}

func serveApplication() {
  router := routers.InitRouter()

  http.ListenAndServe(":8080", router)

	// router := gin.Default()

	// publicRoutes := router.Group("/auth")
	// publicRoutes.POST("/register", controller.Register)
	// publicRoutes.POST("/login", controller.Login)

	// protectedRoutes := router.Group("/api")
	// protectedRoutes.Use(middleware.JWTAuthMiddleware())
	// protectedRoutes.POST("/entry", controller.AddEntry)
	// protectedRoutes.GET("/entry", controller.GetAllEntries)

	// router.Run(":8000")
	fmt.Println("Server running on port 8080")
}
