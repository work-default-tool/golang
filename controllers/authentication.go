package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"golang-api/helper"
	"golang-api/models"
)

type AuthenticationInput struct {
	Username string `json:"username" binding:"required" example:"Jacky"`
	Password string `json:"password" binding:"required" example:"123456"`
}

// Register adds an user from JSON received in the request body.
// @Summary register
// @Schemes
// @Description register user
// @Tags			auth
// @Accept json
// @Produce		json
//
// @Param			request	body	AuthenticationInput	true	"AuthenticationInput"
//
// @Success		201			{string}	string
// @Failure		400
// @Router /auth/register [post]
func Register(context *gin.Context) {
	var input AuthenticationInput

	if err := context.ShouldBindJSON(&input); err != nil {

		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := models.User{
		Username: input.Username,
		Password: input.Password,
	}

	savedUser, err := user.Save()

	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	context.JSON(http.StatusCreated, gin.H{"user": savedUser})
}

// Login user login from JSON received in the request body.
// @Summary login
// @Schemes
// @Description login user
// @Tags			auth
// @Accept json
// @Produce		json
//
// @Param			request	body	AuthenticationInput	true	"AuthenticationInput"
//
// @Success		200			{string}	string
// @Failure		400
// @Router /auth/login [post]
func Login(context *gin.Context) {
	var input AuthenticationInput

	if err := context.ShouldBindJSON(&input); err != nil {

		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user, err := models.FindUserByUsername(input.Username)

	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err = user.ValidatePassword(input.Password)

	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	jwt, err := helper.GenerateJWT(user)
	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	context.JSON(http.StatusOK, gin.H{"jwt": jwt})
}