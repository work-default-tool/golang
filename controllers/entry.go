package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"golang-api/helper"
	"golang-api/models"
)

// AddEntry adds an entry from JSON received in the request body.
// @Summary AddEntry
// @Schemes
// @Description add entry
// @Tags			entry
// @Accept json
// @Produce		json
//
// @Security		ApiKeyAuth
//
// @Param			request	body	models.Entry	true	"Entry"
//
// @Success		201			{string}	string
// @Failure		400
// @Router /api/entry [post]
func AddEntry(context *gin.Context) {
	var input models.Entry
	if err := context.ShouldBindJSON(&input); err != nil {

		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user, err := helper.CurrentUser(context)

	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	input.UserID = user.ID

	savedEntry, err := input.Save()

	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	context.JSON(http.StatusCreated, gin.H{"data": savedEntry})
}

// GetAllEntries responds with the list of all entrys as JSON.
// @Summary GetAllEntries
// @Schemes
// @Description get entryies
// @Tags entry
// @Accept json
// @Produce json
// @Security		ApiKeyAuth
// @Success 200 {string} ok
// @Router /api/entry [get]
func GetAllEntries(context *gin.Context) {

	user, err := helper.CurrentUser(context)

	if err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	context.JSON(http.StatusOK, gin.H{"data": user.Entries})
}