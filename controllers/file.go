package controllers

import (
	"net/http"
	"log"

	"github.com/gin-gonic/gin"
)

// Upload
//
// @Summary Upload file
// @Schemes
// @Description Upload file
// @Tags upload
// @Accept multipart/form-data
// @Produce	json
//
// @Security ApiKeyAuth
//
// @Param			file	formData	file			true	"this is a test file"
// @Success		200		{string}	string			"ok"
//
// @Router /api/upload [post]
func Upload(c *gin.Context) {

	// single file
	file, _ := c.FormFile("file")

	log.Println(file.Filename)

	// Upload the file to specific dst.
	c.SaveUploadedFile(file, "upload/image/" + file.Filename)

	// c.String(http.StatusOK, fmt.Sprintf("'%s' uploaded!", file.Filename))

	c.JSON(http.StatusOK, gin.H{"result": "upload success"})
}