package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// album represents data about a record album.
type album struct {
  ID     string  `json:"id"`
  Title  string  `json:"title"`
  Artist string  `json:"artist"`
  Price  float64 `json:"price"`
}
  
// albums slice to seed record album data.
var albums = []album{
  {ID: "1", Title: "Blue Train", Artist: "John Coltrane", Price: 56.99},
  {ID: "2", Title: "Jeru", Artist: "Gerry Mulligan", Price: 17.99},
  {ID: "3", Title: "Sarah Vaughan and Clifford Brown", Artist: "Sarah Vaughan", Price: 39.99},
}
  
// GetAlbums responds with the list of all albums as JSON.
// @Summary GetAlbums
// @Schemes
// @Description get albums
// @Tags album
// @Accept json
// @Produce json
// @Success 200 {string} ok
// @Router /albums [get]
func GetAlbums(c *gin.Context) {
  c.IndentedJSON(http.StatusOK, albums)
}
  
// PostAlbums adds an album from JSON received in the request body.
// @Summary createAlbum
// @Schemes
// @Description create album
// @Tags			album
// @Accept json
// @Produce		json
//
// @Security		ApiKeyAuth
//
// @Param			request	body	album	true	"album"
//
// @Success		200			{string}	string
// @Failure		400
// @Router /albums [post]
func PostAlbums(c *gin.Context) {
  var newAlbum album
  
  // Call BindJSON to bind the received JSON to
  // newAlbum.
  if err := c.BindJSON(&newAlbum); err != nil {
	return
  }
  
  // Add the new album to the slice.
  albums = append(albums, newAlbum)
  c.IndentedJSON(http.StatusCreated, newAlbum)
}
  
// GetAlbumByID locates the album whose ID value matches the id
// parameter sent by the client, then returns that album as a response.
// @Summary GetAlbumByID
// @Description get album by ID
// @Tags album
// @Accept  json
// @Produce  json
// @Security	ApiKeyAuth
// @Param   id     path    int     true        "Album ID"
// @Success 200 {string} string    "ok"
// @Router /albums/{id} [get]
func GetAlbumByID(c *gin.Context) {
  id := c.Param("id")
  
	// Loop through the list of albums, looking for
	// an album whose ID value matches the parameter.
	for _, a := range albums {
		if a.ID == id {
			c.IndentedJSON(http.StatusOK, a)
		return
	}
  }
  c.IndentedJSON(http.StatusNotFound, gin.H{"message": "album not found"})
}