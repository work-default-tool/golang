package routers

import (
  "github.com/gin-gonic/gin"

  _ "golang-api/docs"
  swaggerfiles "github.com/swaggo/files"
  ginSwagger "github.com/swaggo/gin-swagger"

  "golang-api/controllers"
  "golang-api/middleware"
)

// InitRouter initialize routing information
func InitRouter() *gin.Engine {

  router := gin.Default()

  router.GET("/albums", controllers.GetAlbums)
  router.GET("/albums/:id", controllers.GetAlbumByID)
  router.POST("/albums", controllers.PostAlbums)

  publicRoutes := router.Group("/auth")
	publicRoutes.POST("/register", controllers.Register)
	publicRoutes.POST("/login", controllers.Login)

	protectedRoutes := router.Group("/api")
	protectedRoutes.Use(middleware.JWTAuthMiddleware())
	protectedRoutes.POST("/entry", controllers.AddEntry)
	protectedRoutes.GET("/entry", controllers.GetAllEntries)

  protectedRoutes.POST("/upload", controllers.Upload)

  router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))

  return router
}